package com.krzysztofcywinski.compassapp.presenter;

import com.krzysztofcywinski.compassapp.injector.scope.PerActivity;

import javax.inject.Inject;

@PerActivity
public class UnbindablePresenter<V> implements Presenter<V> {

    protected V view;

    @Inject
    public UnbindablePresenter(){}

    @Override
    public void unbindView() {
        view = null;
    }

    @Override
    public void bindView(V view) {
        this.view = view;
    }

}
