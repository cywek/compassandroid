package com.krzysztofcywinski.compassapp.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.krzysztofcywinski.compassapp.R;

public class CoordinateDialogFragment extends DialogFragment {

	private static final String KEY_COORDINATE_TYPE = "coord_type";

    public enum CoordinateType {
        LATITUDE, LONGITUDE
    }

    private CoordinateType coordinateType;
	private EditText input;
	private CoordinatesListener coordinatesListener;
	private TextInputLayout inputInputLayout;

	public static CoordinateDialogFragment newInstance(CoordinateType coordinateType) {
		CoordinateDialogFragment coordinateDialogFragment = new CoordinateDialogFragment();
		Bundle bundle = new Bundle();
		bundle.putInt(KEY_COORDINATE_TYPE, coordinateType.ordinal());
		coordinateDialogFragment.setArguments(bundle);
		return coordinateDialogFragment;
	}

	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.dialog_layout, null);
		input = view.findViewById(R.id.input);
		inputInputLayout = view.findViewById(R.id.inputLayout);

		Bundle bundle = getArguments();
		coordinateType = CoordinateType.values()[bundle.getInt(KEY_COORDINATE_TYPE)];
		if (coordinateType == CoordinateType.LATITUDE) {
			input.setHint(R.string.latitude);
		} else if (coordinateType == CoordinateType.LONGITUDE) {
			input.setHint(R.string.longitude);
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(view);
		builder.setPositiveButton(android.R.string.ok, (dialog, id) -> {
        }).setNegativeButton(android.R.string.cancel, (dialog, id) -> dialog.cancel());

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> setValueClicked(alertDialog));
        return alertDialog;
	}

    private void setValueClicked(AlertDialog alertDialog) {
        float value = Float.parseFloat(input.getText().toString());
        if (coordinateType == CoordinateType.LATITUDE) {
            if (value >= -90 && value <= 90) {
                coordinatesListener.onLatitudeSet(value);
                alertDialog.dismiss();
            } else {
                inputInputLayout.setError(getString(R.string.error_set_destination_latitude));
            }
        } else if (coordinateType == CoordinateType.LONGITUDE) {
            if (value >= -180 && value <= 180) {
                coordinatesListener.onLongitudeSet(value);
                alertDialog.dismiss();
            } else {
                inputInputLayout.setError(getString(R.string.error_set_destination_longitude));
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        coordinatesListener = (CoordinatesListener) context;
    }

	@Override
	public void onDetach() {
		super.onDetach();
		coordinatesListener = null;
	}

	public interface CoordinatesListener {
		void onLatitudeSet(float latitude);

		void onLongitudeSet(float longitude);
	}
}

