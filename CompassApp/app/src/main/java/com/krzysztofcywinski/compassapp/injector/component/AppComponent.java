package com.krzysztofcywinski.compassapp.injector.component;


import com.krzysztofcywinski.compassapp.MyApplication;
import com.krzysztofcywinski.compassapp.injector.module.AppModule;
import com.krzysztofcywinski.compassapp.injector.scope.PerApp;

import dagger.Component;

@PerApp
@Component(modules = {AppModule.class})
public interface AppComponent {

    /**
     *
     * @return Application dependency.
     */
    MyApplication app();

    /**
     * Injects dependencies into {@link MyApplication}
     *
     * @param app {@link MyApplication}
     */
    void inject(MyApplication app);
}
