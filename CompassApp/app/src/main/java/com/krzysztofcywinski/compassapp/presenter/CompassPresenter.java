package com.krzysztofcywinski.compassapp.presenter;

import android.hardware.GeomagneticField;
import android.hardware.SensorManager;
import android.location.Location;

import com.krzysztofcywinski.compassapp.injector.scope.PerActivity;

import javax.inject.Inject;


@PerActivity
public class CompassPresenter extends CompassContract.CompassPresenter {

//    @Inject
//    MyApplication app;

    private Location lastLocation;
    private GeomagneticField geomagneticField;
    private boolean longitudeSet;
    private boolean latitudeSet;
    private float bearingLatitude = 0f;
    private float bearingLongitude = 0f;
    private float[] gravity;
    private float[] geomagnetic;
    private float north;
    private float bearingDegrees;

    @Inject
    public CompassPresenter() {
    }

    @Override
    public void init() {

        view.setUpAnimator();

    }

    public void onLocationChanged(Location location) {
        this.lastLocation = location;
        updateGeomagneticField();
        view.setLatitude(String.format("%.2f", location.getLatitude()));
        view.setLongitude(String.format("%.2f", location.getLongitude()));
    }


    private void updateGeomagneticField() {
        geomagneticField = new GeomagneticField((float) lastLocation.getLatitude(),
                (float) lastLocation.getLongitude(), (float) lastLocation.getAltitude(),
                lastLocation.getTime());
    }

    public float getBearingDegrees() {
        bearingDegrees = 0f;
        if (lastLocation != null) {
            Location destLocation = new Location(lastLocation);
            destLocation.setLatitude(bearingLatitude);
            destLocation.setLongitude(bearingLongitude);
            bearingDegrees = lastLocation.bearingTo(destLocation);
        }
        return bearingDegrees;
    }

    public float computeTrueNorth(float heading) {
        if (geomagneticField != null) {
            return heading + geomagneticField.getDeclination();
        } else {
            return heading;
        }
    }

    public void onLongitudeSet(float longitude) {
        longitudeSet = true;
        bearingLongitude = longitude;
        view.setDestinationLongitude("" + longitude);
    }

    public void onLatitudeSet(float latitude) {
        latitudeSet = true;
        bearingLatitude = latitude;
        view.setDestinationLatitude("" + latitude);
    }

    public void onAccelerometerChanged(float[] values) {
        gravity = values;
        computeAnimation();
    }

    public void onMagneticFieldChanged(float[] values) {
        geomagnetic = values;
        computeAnimation();
    }

    private void computeAnimation() {
        if (gravity != null && geomagnetic != null) {
            float R[] = new float[9];
            float I[] = new float[9];
            boolean success = SensorManager.getRotationMatrix(R, I, gravity, geomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                float azimut = orientation[0];

                final float end = computeTrueNorth(-azimut * 360 / (2 * 3.14159f));

                float start = north;
                float distance = Math.abs(end - start);
                float reverseDistance = 360.0f - distance;
                float goal;

                if (distance < reverseDistance) {
                    goal = end;
                } else if (end < start) {
                    goal = end + 360.0f;
                } else {
                    goal = end - 360.0f;
                }
                view.animateTo(goal, start);
            }
        }
    }

    @Override
    public void onOrientationChange(float north) {
        this.north = north;
        view.setNeedleRotation(north);
        if (latitudeSet && longitudeSet) {
            view.showArrow();
        }
        view.setRotation(north + getBearingDegrees());
    }
}
