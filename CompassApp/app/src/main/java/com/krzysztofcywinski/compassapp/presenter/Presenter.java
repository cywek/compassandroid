package com.krzysztofcywinski.compassapp.presenter;

public interface Presenter<V> {

    /**
     * Bind view to control by presenter.
     *
     * @param view View to control by presenter.
     */
    void bindView(V view);

    void unbindView();
}
