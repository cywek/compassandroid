package com.krzysztofcywinski.compassapp.injector.component;


import com.krzysztofcywinski.compassapp.view.CompassActivity;
import com.krzysztofcywinski.compassapp.injector.scope.PerActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = {AppComponent.class})
public interface ActivityComponent {

    void inject(CompassActivity activity);

}
