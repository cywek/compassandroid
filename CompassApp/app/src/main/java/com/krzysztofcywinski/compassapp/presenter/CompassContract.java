package com.krzysztofcywinski.compassapp.presenter;

public interface CompassContract {

    interface CompassPresenterView {

        void setLatitude(String lat);

        void setLongitude(String longitude);

        void setUpAnimator();

        void setDestinationLongitude(String longitude);

        void animateTo(float value, float north);

        void setNeedleRotation(float north);

        void showArrow();

        void setRotation(float bearingDegrees);

        void setDestinationLatitude(String lat);
    }
    abstract class CompassPresenter extends UnbindablePresenter<CompassPresenterView> {

        public abstract void init();


        public abstract void onOrientationChange(float north);
    }
}