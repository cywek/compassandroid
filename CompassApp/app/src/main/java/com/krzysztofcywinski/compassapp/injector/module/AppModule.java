package com.krzysztofcywinski.compassapp.injector.module;

import com.krzysztofcywinski.compassapp.MyApplication;
import com.krzysztofcywinski.compassapp.injector.scope.PerApp;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final MyApplication application;

    public AppModule(MyApplication application) {
        this.application = application;
    }

    @PerApp
    @Provides
    public MyApplication provideApp() {
        return application;
    }
}
