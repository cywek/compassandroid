package com.krzysztofcywinski.compassapp.view;

import android.Manifest;
import android.animation.ValueAnimator;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.krzysztofcywinski.compassapp.R;
import com.krzysztofcywinski.compassapp.presenter.CompassContract;
import com.krzysztofcywinski.compassapp.presenter.CompassPresenter;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class CompassActivity extends BaseActivity implements SensorEventListener,
        CoordinateDialogFragment.CoordinatesListener, CompassContract.CompassPresenterView {

    public static final int ANIMATION_DURATION = 200;

    @BindView(R.id.latitude)
    TextView latitude;

    @BindView(R.id.longitude)
    TextView longitude;

    @BindView(R.id.dest_latitude)
    TextView destLatitude;

    @BindView(R.id.dest_longitude)
    TextView destLongitude;

    @BindView(R.id.needle)
    View needle;

    @BindView(R.id.bearing_needle)
    View bearingArrow;

    private static final long MIN_DISTANCE_BETWEEN_LOCATONS = 2;

    private static final long LOCATIONS_INTERVAL = TimeUnit.SECONDS.toMillis(3);

    private ValueAnimator animator;

    private Sensor accelerometer;
    private Sensor magnetometer;
    private SensorManager sensorManager;
    private LocationManager locationManager;

    @Inject
    CompassPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        initDaggerComponents();
        initViews();

        presenter.init();

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
    }

    @Override
    protected void onDestroy() {
        presenter.unbindView();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
        sensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_UI);
        setupLocationManagerWithPermission();
    }

    protected void onPause() {
        super.onPause();

        sensorManager.unregisterListener(this);

        locationManager.removeUpdates(locationListener);
    }

    @Override
    public void onLatitudeSet(float latitude) {
        presenter.onLatitudeSet(latitude);
    }

    @Override
    public void onLongitudeSet(float longitude) {
        presenter.onLongitudeSet(longitude);
    }

    @OnClick(R.id.latitudeButton)
    public void onLatClick(View v) {
        CoordinateDialogFragment coordinateDialogFragment = CoordinateDialogFragment.newInstance(
                CoordinateDialogFragment.CoordinateType.LATITUDE);
        coordinateDialogFragment.show(getSupportFragmentManager(), "inputDialog");
    }

    @OnClick(R.id.longitudeButton)
    public void onLonClick(View v) {
        CoordinateDialogFragment coordinateDialogFragment = CoordinateDialogFragment.newInstance(
                CoordinateDialogFragment.CoordinateType.LONGITUDE);
        coordinateDialogFragment.show(getSupportFragmentManager(), "inputDialog");
    }

    @Override
    public void setLatitude(String lat) {
        latitude.setText(lat);
    }

    @Override
    public void setLongitude(String longitude) {
        this.longitude.setText(longitude);
    }

    protected void initDaggerComponents() {
        ActivityComponentInitializer.init().inject(this);
    }

    protected void initViews() {
        presenter.bindView(this);
    }

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            presenter.onLocationChanged(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            presenter.onAccelerometerChanged(sensorEvent.values);
        }
        if (sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            presenter.onMagneticFieldChanged(sensorEvent.values);
    }

    @Override
    public void animateTo(float goal, float start) {
        if (!animator.isRunning()) {
            animator.setFloatValues(start, goal);
            animator.start();

        }
    }

    @Override
    public void setNeedleRotation(float north) {
        needle.setRotation(north);
    }

    @Override
    public void showArrow() {
        bearingArrow.setVisibility(View.VISIBLE);
    }

    @Override
    public void setRotation(float bearingDegrees) {
        bearingArrow.setRotation(bearingDegrees);
    }

    @Override
    public void setDestinationLatitude(String lat) {
        destLatitude.setText(lat);
    }

    @SuppressWarnings("MissingPermission")
    public void setupLocationManagerWithPermission() {

        RxPermissions rxPermissions = new RxPermissions(this);

        rxPermissions.request(Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribe(granted -> {
                    if (granted) { // Always true pre-M
                        setUpLocationManager();
                    } else {
                        Toast.makeText(CompassActivity.this, getString(R.string.permission_denied), Toast.LENGTH_LONG).show();

                    }
                });
    }

    @SuppressWarnings("MissingPermission")
    private void setUpLocationManager() {
        Location lastLocation = locationManager
                .getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        if (lastLocation != null) {
            presenter.onLocationChanged(lastLocation);
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                LOCATIONS_INTERVAL, MIN_DISTANCE_BETWEEN_LOCATONS, locationListener, Looper.getMainLooper());
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    @Override
    public void setUpAnimator() {
        animator = new ValueAnimator();
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration(ANIMATION_DURATION);
        animator.addUpdateListener(animation -> {
            float north = (float) animation.getAnimatedValue();
            presenter.onOrientationChange(north);
        });
    }

    @Override
    public void setDestinationLongitude(String longitude) {
        destLongitude.setText(longitude);
    }

}
