package com.krzysztofcywinski.compassapp.view;

import android.support.v7.app.AppCompatActivity;

import com.krzysztofcywinski.compassapp.MyApplication;
import com.krzysztofcywinski.compassapp.injector.component.ActivityComponent;
import com.krzysztofcywinski.compassapp.injector.component.DaggerActivityComponent;

public abstract class BaseActivity extends AppCompatActivity {

    /**
     * Initializer for {@link ActivityComponent}
     */
    protected static class ActivityComponentInitializer {

        private ActivityComponentInitializer() {
        }

        /**
         * Init injector component.
         *
         * @return {@link ActivityComponent}
         */
        public static ActivityComponent init() {
            return DaggerActivityComponent.builder()
                    .appComponent(MyApplication.getAppComponent())
                    .build();
        }
    }

    /**
     * Initialize dagger component
     */
    protected abstract void initDaggerComponents();

    /**
     * Initialize views
     */
    protected abstract void initViews();

}